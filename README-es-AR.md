# Keithley-SMU-helper-scripts

This README is also available in: [English](README.md)

Este repo contiene algunos scripts Test Script Processor (TSP) para SMUs Keithley 2600 series.

Los scripts incluidos son:

- FechaYHora: te permite corregir la fecha y hora del equipo.
- BorraScripts: te permite borrar scripts guardados en la memoria interna desde el planel frontal.

# ToDo

- FechaYHora: permitir elegir el huso horario (por default GMT-3)

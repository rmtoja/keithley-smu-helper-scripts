# Keithley-SMU-helper-scripts

Este LEEME está también disponible en: [Castellano](README-es-AR.md)

This repo contains some Test Script Processor (TSP) scripts for a Keithley 2600 series SMU.

The included scripts are:

- DateAndTime: helps you input correct time and date info.
- ScriptDeleter: allows you to delete scripts saved in internal memory from the front panel.

# ToDo

- DateAndTime: allow to select the timezone (default is GMT-3)
